# Network Management

UniFi controller for configuring UniFi network gear within hQ

## Obsolete

This functionality of this repo has been made obsolete by running a dedicated cloud key instead.
This means I no longer need to self host the UniFi Controller in order to configure the network.
This repo will remain listed as an example of how I used to do it.

## CI/CD

This repository loads its CI/CD pipeline from a common template found in the
[Job Template](https://gitlab.com/hq-smarthome/home-lab/job-template) repository
