job "network-management" {
  type = "service"
  region = "global"
  datacenters = ["proxima"]

  priority = 70

  constraint {
    attribute = "${attr.unique.hostname}"
    value = "proxima-a"
  }

  group "unifi-controller" {
    count = 1

    volume "unifi-controller" {
      type = "host"
      read_only = false
      source = "unifi-controller"
    }

    network {
      mode = "bridge"

      port "stun" { 
        static = "3478"
        to = "3478"
      }

      port "device-discovery" {
        static = "10001"
        to = "10001"
      }

      port "device-communication" {
        static = "8080"
        to = "8080"
      }

      port "web" {
        to = "8443"
      }
    }

    task "controller" {
      driver = "docker"

      volume_mount {
        volume = "unifi-controller"
        destination = "/config"
        read_only = false
      }

      config {
        image = "[[ .unifiImage ]]"

        ports = ["stun", "device-communication", "device-discovery", "web"]
      }

      resources {
        cpu = 1536
        memory = 1536
      }


      service {
        name = "unifi"
        port = "web"

        tags = [
          "traefik.enable=true",
          "traefik.http.routers.unifi.entrypoints=https",
          "traefik.http.routers.unifi.tls=true",
          "traefik.http.routers.unifi.tls.certresolver=lets-encrypt",
          "traefik.http.routers.unifi.tls.domains[0].main=*.hq.carboncollins.se",
          "traefik.http.services.unifi.loadBalancer.serversTransport=insecureSkipVerify@file",
          "traefik.http.services.unifi.loadbalancer.server.scheme=https"
        ]
      }
    }
  }

  reschedule {
    delay = "10s"
    delay_function = "exponential"
    max_delay = "10m"
    unlimited = true
  }

  update {
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "10m"
    progress_deadline = "15m"
    auto_revert = true
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
    statefull = "true"
  }
}
